#include <iostream>
#include "nlohmann/json.hpp"

using json = nlohmann::json;
using namespace std;

int main()
{
    // create an empty structure (null)
    json j;

    // add a number that is stored as double (note the implicit conversion of j to an object)
    j["pi"] = 3.141;

    // add a Boolean that is stored as bool
    j["happy"] = true;

    string s = j.dump(); 
    cout << s << endl;
}
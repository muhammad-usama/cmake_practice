cmake_minimum_required(VERSION 3.16)
project(Json)

add_subdirectory(json)
link_libraries(nlohmann_json::nlohmann_json)
add_executable(${PROJECT_NAME} main.cpp)
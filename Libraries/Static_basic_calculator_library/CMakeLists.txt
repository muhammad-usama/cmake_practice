cmake_minimum_required(VERSION 3.16)
project(basic_calculator)

set(CMAKE_BUILD_TYPE release)

#Can manually add the sources using the set command as follows:
#set(SOURCES src/main.cpp src/basic_calculator.cpp)
 
#However, the file(GLOB...) allows for wildcard additions:
file(GLOB SOURCES "src/*.cpp")

#Generate the static library from the sources
add_library(${PROJECT_NAME} STATIC ${SOURCES} )

# Set the location for library installation -- i.e., /usr/lib in this case
install(TARGETS ${PROJECT_NAME} DESTINATION /usr/lib)
install(FILES include/basic_calculator.h DESTINATION /usr/include)
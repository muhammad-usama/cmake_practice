# !/bin/bash

if [ ! -d "$(pwd)/build" ] ; then
    mkdir build
fi

cd build
cmake ..
cmake --build .
sudo cmake --install .

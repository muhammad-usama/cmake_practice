cmake_minimum_required(VERSION 3.16)
project(Calculator_using_directories)

#Bring the headers, such as basic_calculator.h into the project
include_directories(include)
 
#Can manually add the sources using the set command as follows:
#set(SOURCES src/main.cpp src/basic_calculator.cpp)
 
#However, the file(GLOB...) allows for wildcard additions:
file(GLOB SOURCES "src/*.cpp")
 
add_executable(calculator ${SOURCES})